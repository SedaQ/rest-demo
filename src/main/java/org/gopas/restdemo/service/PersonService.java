package org.gopas.restdemo.service;

import org.gopas.restdemo.api.Person;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService {

    public List<Person> findAll() {
        List<Person> persons = new ArrayList<>();
        Person person1 = new Person(1L, "myemail1", "test");
        Person person2 = new Person(2L, "myemail1", "test");
        persons.add(person1);
        persons.add(person2);
        return persons;
    }
}
